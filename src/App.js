import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './pages/Home/Home';
import BattleshipFiled from './pages/BattleshipField/BattleshipFiled';
import Rules from './pages/Rules/Rules';


function App() {
  return (
    <Router>
      <Routes>
        <Route index element={<Home />} />
        <Route path='/battleship-field' element={<BattleshipFiled />} />
        <Route path='/rules' element={<Rules />} />
      </Routes>
    </Router>
  );
}

export default App;
