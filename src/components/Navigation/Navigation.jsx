import React from 'react';
import './Navigation.css';
import { useNavigate } from 'react-router-dom';

const Navigation = () => {
  const navigate = useNavigate();

  return (
    <>
      <div className='navigation-container'>
        <button
          className='button nav-goToBattle'
          onClick={() => navigate('/battleship-field')}>
          Go to Battle!
        </button>
        <button className='button nav-rules' onClick={() => navigate('/rules')}>
          Rules!
        </button>
      </div>
    </>
  );
};

export default Navigation;
