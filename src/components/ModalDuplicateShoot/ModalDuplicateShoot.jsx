import React from 'react';
import './ModalDuplicateShoot.css';

const ModalDuplicateShoot = ({ setModalDuplicateShoot }) => {
  return (
    <div className='backShadow'>
      <div className='setModalDuplicateShoot-container'>
        <div className='setModalDuplicateShoot-warning'>
          You have already shot at this location!
        </div>
        <button
          className='button'
          onClick={() => setModalDuplicateShoot(false)}>
          Close
        </button>
      </div>
    </div>
  );
};

export default ModalDuplicateShoot;
