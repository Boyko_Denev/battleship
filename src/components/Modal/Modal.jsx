import React from 'react';
import './Modal.css';

const Modal = ({ setModal, infoCount, setInfoCount }) => {
  const handleClick = () => {
    setInfoCount(0);
    setModal(false);
  };
  return (
    <div className='backShadow'>
      <div className='modal-container'>
        <div className='modal-congrats'>Well done!</div>
        <div className='modal-message'>
          You completed the game in {infoCount} shots.
        </div>

        <button className='button' onClick={handleClick}>
          Close
        </button>
      </div>
    </div>
  );
};

export default Modal;
