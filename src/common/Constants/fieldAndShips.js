export const SIZE_MAP = 10;

export const SHIPS_TYPE_SIZE = [[1, 5], [2, 4]];  // 1 battleship size 5 and 2 destroyers size 4

export const END_HIT_POINTS = 13;   // Sum of total ships sizes