export const VALIDATE_VERTICAL = '^[A-Ja-j]{1,1}$'

export const VALIDATE_HORIZONTAL = '^(?:[1-9]|0[1-9]|10)$'