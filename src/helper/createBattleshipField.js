import { SHIPS_TYPE_SIZE, SIZE_MAP } from "../common/Constants/fieldAndShips";

const BattleField = () => {
  const battleField = [];

  for (let row = 0; row < SIZE_MAP; row++) {
    battleField[row] = new Array(SIZE_MAP).fill(0);
  }

  const getRandomInt = (max) => {
    return Math.floor(Math.random() * max);
  }

  // get directions (from 0 to 3). 0-up / 1-right / 2-down / 3-left
  const getRandomDirections = () => {
    const directions = []
    for (let i = 0; directions.length < 4; i++) {
      let tempNumber = getRandomInt(4)
      if (directions.indexOf(tempNumber) === -1) {
        directions.push(tempNumber)
      }
    }

    return directions
  }

  const createShip = (size) => {

    const directions = getRandomDirections();

    let validCreateShip = false;

    for (let i = 0; i < Math.pow(SIZE_MAP, 2); i++) {
      const row = getRandomInt(SIZE_MAP - 1);
      const col = getRandomInt(SIZE_MAP - 1);

      for (let j = 0; j < directions.length; j++) {

        // up
        if (directions[j] === 0) {
          let validPosition = true

          for (let k = 0; k < size; k++) {

            if (row - k < 0 || battleField[row - k][col] !== 0) {
              validPosition = false
            }
          }

          if (validPosition) {
            for (let k = 0; k < size; k++) {
              battleField[row - k][col] = 1
            }
            validCreateShip = true;

            break;
          }
        }

        // right
        if (directions[j] === 1) {
          let validPosition = true

          for (let k = 0; k < size; k++) {

            if (col + k >= SIZE_MAP || battleField[row][col + k] !== 0) {
              validPosition = false
            }
          }

          if (validPosition) {
            for (let k = 0; k < size; k++) {
              battleField[row][col + k] = 1
            }
            validCreateShip = true;

            break;
          }
        }

        // down
        if (directions[j] === 2) {
          let validPosition = true

          for (let k = 0; k < size; k++) {

            if (row + k >= SIZE_MAP || battleField[row + k][col] !== 0) {
              validPosition = false
            }
          }

          if (validPosition) {
            for (let k = 0; k < size; k++) {
              battleField[row + k][col] = 1
            }
            validCreateShip = true;

            break;
          }
        }

        // left
        if (directions[j] === 3) {
          let validPosition = true

          for (let k = 0; k < size; k++) {

            if (col - k < 0 || battleField[row][col - k] !== 0) {
              validPosition = false
            }
          }

          if (validPosition) {
            for (let k = 0; k < size; k++) {
              battleField[row][col - k] = 1
            }
            validCreateShip = true;

            break;
          }
        }
      }

      if (validCreateShip) {

        break;
      }
    }
  }

  for (let i = 0; i < SHIPS_TYPE_SIZE.length; i++) {
    for (let j = 0; j < SHIPS_TYPE_SIZE[i][0]; j++) {
      createShip(SHIPS_TYPE_SIZE[i][1])
    }
  }

  return battleField
}

export default BattleField;