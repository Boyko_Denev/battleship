import React from 'react';
import './Rules.css';
import { useNavigate } from 'react-router-dom';

const Rules = () => {
  const navigate = useNavigate();

  return (
    <div className='rules-background'>
      <div className='rules-container'>
        <div className='rules-title'>Description</div>
        <p>
          The game is played on grid 10x10. This version of the game is for
          single player. The computer generate and arrange 3 ships: 1 x
          Battleship (5 squares) and 2 x Destroyers (4 squares). Ships can touch
          but not overlap. Type coordinates and shoot! Your goal is to find the
          enemy ships and sink them.
        </p>
        <div className='rules-title'>History</div>
        <p>
          The game of Battleship is thought to have its origins in the French
          game L'Attaque played during World War I, although parallels have also
          been drawn to E. I. Horsman's 1890 game Basilinda, and the game is
          said to have been played by Russian officers before World War I. The
          first commercial version of the game was Salvo, published in 1931 in
          the United States by the Starex company. Other versions of the game
          were printed in the 1930s and 1940s, including the Strathmore
          Company's Combat: The Battleship Game, Milton Bradley's Broadsides: A
          Game of Naval Strategy and Maurice L. Freedman's Warfare Naval Combat.
          Strategy Games Co. produced a version called Wings which pictured
          planes flying over the Los Angeles Coliseum. All of these early
          editions of the game consisted of pre-printed pads of paper.
        </p>
        <div className='rules-button'>
          <button className='button' onClick={() => navigate('/')}>
            Back
          </button>
        </div>
      </div>
    </div>
  );
};

export default Rules;
