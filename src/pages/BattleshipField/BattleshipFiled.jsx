import React, { useState } from 'react';
import './BattleshipFiled.css';
import BattleField from '../../helper/createBattleshipField';
import { useNavigate } from 'react-router-dom';
import { END_HIT_POINTS, SIZE_MAP } from '../../common/Constants/fieldAndShips';
import {
  VALIDATE_HORIZONTAL,
  VALIDATE_VERTICAL,
} from '../../common/Constants/validationFields';
import Modal from '../../components/Modal/Modal';
import ModalDuplicateShoot from '../../components/ModalDuplicateShoot/ModalDuplicateShoot';
import Ship01 from '../../assets/images/ship-on-map.jpg';

const BattleshipFiled = () => {
  const navigate = useNavigate();
  const [dataBattleField, setDataBattleField] = useState([]);
  const [faceBattleField, setFaceBattleField] = useState([]);
  const [activeBattle, setActiveBattle] = useState(false);
  const [infoText, setInfoText] = useState("Let's start");
  const [infoCount, setInfoCount] = useState(0);
  const [hitPoints, setHitPoints] = useState(0);
  const [modal, setModal] = useState(false);
  const [modalDuplicateShoot, setModalDuplicateShoot] = useState(false);
  const battleFieldColHeader = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const battleFieldRowHeader = [
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
  ];

  const [coordinates, setCoordinates] = useState({
    vertical: '',
    horizontal: '',
  });

  const handleInputChange = (e) => {
    setCoordinates({
      ...coordinates,
      [e.target.name]: e.target.value,
    });
  };

  const exitBattle = () => {
    if (!activeBattle) {
      navigate('/');
    } else if (
      window.confirm(
        'Are you sure? If you agree you will not be able to continue this battle!',
      )
    ) {
      navigate('/');
    }
  };

  const startBattle = () => {
    setDataBattleField(BattleField());

    setFaceBattleField(
      Array.from({ length: SIZE_MAP }, () =>
        Array.from({ length: SIZE_MAP }, () => 2),
      ),
    );
    setActiveBattle(true);
  };

  const surrender = () => {
    setCoordinates({
      vertical: '',
      horizontal: '',
    });
    setInfoText("Let's start");
    setInfoCount(0);
    setHitPoints(0);
    setDataBattleField([]);
    setFaceBattleField([]);
    setActiveBattle(false);
  };

  const showMap = () => {
    const squares = [...dataBattleField];
    setFaceBattleField(squares);
  };

  const submitCoordinates = (e) => {
    e.preventDefault();

    const row = coordinates.vertical.toUpperCase().charCodeAt() - 65; // Convert char in index using ascII
    const col = coordinates.horizontal - 1;

    if (faceBattleField[row][col] === 0 || faceBattleField[row][col] === 1) {
      setModalDuplicateShoot(true);
      return;
    }

    const squares = [...faceBattleField];

    if (dataBattleField[row][col] === 1) {
      squares[row][col] = 1;
      setInfoText('You Hit!');
      setHitPoints(hitPoints + 1);
      if (hitPoints + 1 === END_HIT_POINTS) {
        surrender();
        setModal(true);
      }
    } else if (dataBattleField[row][col] === 0) {
      squares[row][col] = 0;
      setInfoText('You Miss!');
    } else {
      squares[row][col] = 2;
    }

    setFaceBattleField(squares);
    setInfoCount(infoCount + 1);
  };

  return (
    <>
      <div className='title-battleField'>Battleship</div>
      <div className='navigation-battleField'>
        <button className='button' onClick={exitBattle}>
          Exit Battle
        </button>
        <button
          className='button'
          disabled={activeBattle}
          onClick={startBattle}>
          Start Battle
        </button>
      </div>

      {activeBattle ? (
        <div className='control-panel'>
          <div className='title-control-panel'>Set shooting coordinates</div>
          <div className='form-container'>
            <form onSubmit={submitCoordinates}>
              <div className='label-input vertical-input'>
                <div className='label-wrapper'>
                  <label htmlFor='vertical'>Vertical</label>
                </div>
                <input
                  type='text'
                  name='vertical'
                  value={coordinates.vertical.toUpperCase()}
                  onChange={handleInputChange}
                  id='vertical'
                  pattern={VALIDATE_VERTICAL}
                  required={true}></input>
                <span>from A to J</span>
              </div>
              <div className='label-input horizontal-input'>
                <div className='label-wrapper'>
                  <label htmlFor='horizontal'>Horizontal</label>
                </div>
                <input
                  type='text'
                  name='horizontal'
                  value={coordinates.horizontal}
                  onChange={handleInputChange}
                  id='horizontal'
                  pattern={VALIDATE_HORIZONTAL}
                  required={true}></input>
                <span>from 1 to 10</span>
              </div>
              <button className='shoot-button submit-input'>SHOOT!</button>
            </form>
          </div>
          <div className='information'>
            <div className='info-text'>{infoText}</div>
            <div className='info-count'>Shots: {infoCount}</div>
          </div>
          <div className='bottom-navigation'>
            <button
              className='button'
              onClick={() => {
                if (
                  window.confirm(
                    'Are you sure? If you agree you will not be able to continue this battle!',
                  )
                )
                  surrender();
              }}>
              Surrender
            </button>
            <button className='button' onClick={showMap}>
              Show Ships
            </button>
          </div>
        </div>
      ) : (
        <></>
      )}

      <div className='grid-container'>
        {!activeBattle ? (
          <img src={Ship01} alt='ship' />
        ) : (
          <>
            <div className='grid-item grid-blank'></div>
            <div className='grid-item grid-col-header'>
              {battleFieldColHeader.map((colHeader, indexCol) => (
                <div key={indexCol} className='colHeaderItem'>
                  {colHeader}
                </div>
              ))}
            </div>
            <div className='grid-item grid-row-header'>
              {battleFieldRowHeader.map((colHeader, indexCol) => (
                <div key={indexCol} className='rowHeaderItem'>
                  {colHeader}
                </div>
              ))}
            </div>
            <div className='grid-item grid-map'>
              {faceBattleField.map((row) =>
                row.map((item, indexCol) => (
                  <div
                    className={`map-element`}
                    key={indexCol}
                    style={
                      item === 0
                        ? { backgroundColor: 'var(--water-color)' }
                        : item === 1
                        ? { backgroundColor: 'var(--boom-color)' }
                        : { backgroundColor: 'var(--light-color)' }
                    }></div>
                )),
              )}
            </div>
          </>
        )}
      </div>
      {modal === true && (
        <Modal
          modal={modal}
          setModal={setModal}
          infoCount={infoCount}
          setInfoCount={setInfoCount}
        />
      )}
      {modalDuplicateShoot === true && (
        <ModalDuplicateShoot setModalDuplicateShoot={setModalDuplicateShoot} />
      )}
    </>
  );
};

export default BattleshipFiled;
