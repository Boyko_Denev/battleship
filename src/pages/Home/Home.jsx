import React from 'react';
import './Home.css';
import ShipWallpaper from '../../assets/images/ship-wallpaper.jpg';
import Title from '../../assets/images/title-battleship.png';
import Navigation from '../../components/Navigation/Navigation';

const Home = () => {
  return (
    <div
      className='home-backgroundImage'
      style={{ backgroundImage: `url(${ShipWallpaper})` }}>
      <div className='home-container'>
        <img className='home-title' src={Title} alt='title-battleship' />
        <div className='home-navigation'>
          <Navigation />
        </div>
      </div>
    </div>
  );
};

export default Home;
